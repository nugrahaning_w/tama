<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Crud {
	function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library( array( 'session' ) );
    }

    function save_data( $tabel, $data = null, $where = null ) {
		if ( is_array( $tabel ) ) {
			if ( ! empty( $tabel['where'] ) ) {
				$this->CI->db->where( $tabel['where'] );
				$id_save = $this->CI->db->update( $tabel['table'], $tabel['data'] );
			} else {
				$this->CI->db->insert( $tabel['table'], $tabel['data'] );
				$id_save = $this->CI->db->insert_id();
			}
		} else {
			if ( ! empty( $where ) ) {
				if ( is_array( $where ) ) {
					if ( count( $where ) > 1 ) {
						foreach ( $where as $col => $val ) {
							$this->CI->db->where( $col, $val );
						}
					} else {
						$this->CI->db->where( $where );
					}
				} else {
					$this->CI->db->where( $where );
				}
				$this->CI->db->update( $tabel, $data );
				$id_save = $this->CI->db->affected_rows();
			} else {
				$this->CI->db->insert( $tabel, $data );
				$id_save = $this->CI->db->insert_id();
			}
		}

		return $id_save;
	}
	
		// DELETE
	function delete_data( $tabel, $column = null, $id = null) {
		if ( is_array( $tabel ) ) {
			foreach( $tabel['where'] as $w => $an ) {
				if ( is_null( $an ) ) $this->CI->db->where( $w, null, false );
				else $this->CI->db->where( $w, $an );
			}
			return $this->CI->db->delete( $tabel['table'] );
		} else {
			if ( ! empty( $column ) ) {
				if ( is_array( $column ) ) $this->CI->db->where( $column );
				else $this->CI->db->where( $column, $id );
			}
			return $this->CI->db->delete( $tabel );
		}
	}
    
    function get_data( $param, $is_array = 0 ) {
		/*
			$param['table'] 		-> string or array
			$param['select']		-> string
			$param['where']			-> array
			$param['group_by']		-> string
			$param['order_by']		-> string
			$param['not_in']		-> array
			$param['limit']			-> int
			$param['offset']		-> int
		*/
		// SELECT
		if ( ! empty( $param['select'] ) ) {
			$this->CI->db->select( $param['select'], false );
		}

		// FROM
		if ( is_array( $param['table'] ) ) {
			$n = 1;
			foreach( $param['table'] as $tab => $on ) {
				if ( $n > 1 ) {
					if ( is_array( $on ) ) {
						$this->CI->db->join( $tab, $on[0], $on[1] );
					} else {
						$this->CI->db->join( $tab, $on );
					}
				} else {
					$this->CI->db->from( $tab );
				}
				$n++;
			}
		} else {
			$this->CI->db->from( $param['table'] );
		}

		// WHERE
		if ( ! empty( $param['where'] ) ) {
			foreach( $param['where'] as $w => $an ) {
				if ( is_null( $an ) ){
					$this->CI->db->where( $w, null, false );
				}else {
					$this->CI->db->where( $w, $an );
				}
			}
		}

		// NOT IN
		if ( ! empty( $param['not_in'] ) ) {
			if ( is_array( $param['not_in'] ) ) {
				foreach( $param['not_in'] as $w => $an ) {
					if ( is_null( $an ) ) {
						$this->CI->db->where_not_in( $w, null, false );
					}else {
						$this->CI->db->where_not_in( $w, $an );
					}
				}
			} else {
				$this->CI->db->where_not_in($param['not_in'],$param['not_in_isi']);
			}
		}

		// LIMIT, OFFSET
		if ( ! empty( $param['limit'] ) && ! empty( $param['offset'] ) ) {
			$this->CI->db->limit( $param['limit'], $param['offset'] );
		} else if ( ! empty( $param['limit'] ) && empty( $param['offset'] ) ) {
			$this->CI->db->limit( $param['limit'] );
		}

		// LIKE
		if ( ! empty( $param['like'] ) ) {
			$srch = [];
			foreach( $param['like'] as $sc => $vl ) {
				if ( $vl != NULL ) $srch[] = $sc . " LIKE '%" . $vl . "%'";
			}

			if ( count( $srch ) > 0 ) $this->CI->db->where( '(' . implode(' OR ', $srch ) . ')', null, false );

		}

		// ORDER_BY
		if ( ! empty( $param['order_by'] ) ) {
			$this->CI->db->order_by( $param['order_by'] );
		}

		// GROUP_BY
		if ( ! empty( $param['group_by'] ) ) {
			$this->CI->db->group_by( $param['group_by'] );
		}
		// $query = $this->CI->db->get();
		return $this->CI->db->get();

		// return ( ( $is_array ) ? $query->result();
    }
    
    function combo_box($param = array()) {
		$combo 		= false;
		if(!empty($param['pilih'])) {
			$combo 		= array(
				'' 			=> !empty($param['pilih']) ? $param['pilih']:'-- Pilih --');
		}
		foreach($param['data_combo'] as $row) {
			$valueb 	= array();

			if (is_array($param['val'])) {
				foreach($param['val'] as $v) {
					if (is_array($v)) {
						if ($v[0] == "(") $valueb[] = "(".$row->$v[1].")";
					} else {
						$valueb[] = $row->$v;
					}
				}
			} else {
				$valueb[] = $row->$param['val'];
			}
			$keyb = array();
			if (is_array($param['key'])) {
				foreach($param['key'] as $k) { $keyb[] = (strlen($row->$k) > 100)?substr($row->$k,0,100).' ...':$row->$k; }
			}
			$keyv = is_array($param['key']) ? implode("|",$keyb) : $row->$param['key'];

			$combo[$keyv] = implode(" ",$valueb);
		} return $combo;
    }
    
    function data_query( $sql = null ){
		return $this->CI->db->query( $sql );
	}
}
?>
