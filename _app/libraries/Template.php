<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template
{

    private $CI;
    private $template_params;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->template_params = array();
    }

    function Minify_Html($buffer)
    {
        $search = array(
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        );

        $replace = array(
            '>',
            '<',
            '\\1',
            ''
        );

        $buffer = preg_replace($search, $replace, $buffer);

        return $buffer;
    }

    public function set($position, $data, $append = true)
    {
        if (!isset($this->template_params[$position]) || $append === false) {
            $this->template_params[$position] = $data;
        } else {
            $this->template_params[$position] .= $data;
        }
    }

    public function get($position)
    {
        if (isset($this->template_params[$position])) {
            return $this->template_params[$position];
        } else {
            return '';
        }
    }

    public function title($title = '')
    {
        $this->template_params['title'] = $title;
    }

    public function meta_description($meta_description = '')
    {
        $this->template_params['meta_description'] = $meta_description;
    }

    public function meta_image($meta_image = '')
    {
        $this->template_params['meta_image'] = $meta_image;
    }

    public function breadcrumb($breadcrumb = '')
    {
        $this->template_params['breadcrumb'] = $breadcrumb;
    }

    public function header_script($header_script = '')
    {
        $this->template_params['header_script'] = $header_script;
    }

    public function footer_script($footer_script = '')
    {
        $this->template_params['footer_script'] = $footer_script;
    }

    public function content($view, $params = array(), $position = 'content', $append = true)
    {

        $data = $this->CI->load->view($view, $params, true);
        $this->set($position, $data, $append);
    }

    public function wrapper($view, $params = array(), $position = 'wrapper', $append = true)
    {
        $data = $this->CI->load->view($view, $params, true);
        $this->set($position, $data, $append);
    }

    public function show($template_view = 'public/template', $return_string = true)
    {
        $this->template_params['addons'] = base_url('assets/addons/');
        $this->template_params['style'] = base_url('assets/style/');
        $data = $this->template_params;
        $complete_page = $this->CI->load->ext_view($template_view, $data, true);
        // $complete_page = $this->Minify_Html($this->CI->load->ext_view($template_view, $data, true));

        if ($return_string == true) {
            echo  $complete_page;
        } else {
            return $complete_page;
        }
    }

    public function blank($view, $params = array())
    {
        $this->CI->load->view($view, $params);
    }
}
