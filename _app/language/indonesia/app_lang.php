<?php
//halaman public

//menu content public
$lang['public_header_menu_shopping'] = 'Belanja';
$lang['public_header_menu_photo'] = 'Foto';
$lang['public_header_menu_livizobusiness'] = 'Livizo Expo';
$lang['public_header_menu_news'] = 'Berita';
$lang['public_header_currency'] = 'Mata Uang';
$lang['public_header_language'] = 'Bahasa';
$lang['public_header_searchproduct'] = 'Cari produk ...';
$lang['public_main_title_brands'] = 'Brands';

//main content public
$lang['public_main_title_icon'] = 'Solusi Semua Kebutuhan Dekorasi dan Furniture Anda !';
$lang['public_main_title_icon_shopping'] = 'Belanja';
$lang['public_main_title_icon_professional'] = 'Professional';
$lang['public_main_title_icon_expo'] = 'Pameran Virtual';
$lang['public_main_icon_shopping'] = 'Hiasi dengan desain produk-produk dari kami yang unik dan terlengkap';
$lang['public_main_icon_professional'] = 'Temukan pelayanan dan jasa untuk kebutuhan personal dan bisnis anda';
$lang['public_main_icon_expo'] = 'Nikmati pameran furnitur virtual dari rumah anda';

$lang['public_main_title_category'] = 'Kategori Belanja';
$lang['public_main_category_living_room'] = 'Ruang Tamu';
$lang['public_main_category_dining_room'] = 'Ruang Makan';
$lang['public_main_category_family_room'] = 'Ruang Keluarga';
$lang['public_main_category_bed_room'] = 'Kamar Tidur';
$lang['public_main_category_home_office'] = 'Ruang Kerja';
$lang['public_main_category_home_decor'] = 'Dekorasi Rumah';
$lang['public_main_category_outdoor'] = 'Outdoor';
$lang['public_main_category_kitchen'] = 'Dapur';
$lang['public_main_title_newproduct'] = 'Produk Terbaru';
$lang['public_main_title_promoproduct'] = 'Produk Promo';
$lang['public_main_title_businessproduct'] = 'Produk Bisnis';

$lang['public_main_title_professional'] = 'Bertemu Dengan Professional';
$lang['public_main_professional'] = 'Desain dan dekorasi rumah dengan profesional';
$lang['public_main_professional_shopnow'] = 'Selengkapnya';

$lang['public_main_title_ife'] = 'Pameran Furniture Online Internasional';
$lang['public_main_date_ife'] = '15 November – 05 Desember 2020';
$lang['public_main_ife'] = 'Mulailah perjalanan pameran virtual Anda bersama IFE 2020';
$lang['public_main_ife_register'] = 'DAFTAR SEKARANG';

$lang['public_main_title_product'] = 'Pilih Produk Sesuai Style dan Selera Anda';
$lang['public_main_product'] = 'Menyediakan Banyak Pilihan Style dari Brand Ternama';
$lang['public_main_product_shopnow'] = 'Belanja Sekarang';

$lang['public_main_title_designchoices'] = 'Desain Pilihan';
$lang['public_main_designchoices_findallideas'] = 'Temukan Semua Ide';

$lang['public_footer_title_help'] = 'Butuh Bantuan ?';

$lang['public_footer_title_navigation'] = 'Navigasi';
$lang['public_footer_list_home'] = 'Beranda';
$lang['public_footer_list_shopping'] = 'Belanja';
$lang['public_footer_list_photo'] = 'Foto';
$lang['public_footer_list_livizobusiness'] = 'Livizo Expo';
$lang['public_footer_list_news'] = 'Berita';

$lang['public_footer_title_aboutus'] = 'Tentang Kami';
$lang['public_footer_list_livizoteam'] = 'Tim Livizo';
$lang['public_footer_list_career'] = 'Karir';
$lang['public_footer_list_policyandprivacy'] = 'Kebijakan dan Privasi';
$lang['public_footer_list_contactus'] = 'Hubungi Kami';

$lang['public_footer_title_sellernprofessional'] = 'Penjual & Professional';
$lang['public_footer_list_regisselleraccount'] = 'Daftar Akun Penjual';
$lang['public_footer_list_sellerlogin'] = 'Login Penjual';
$lang['public_footer_list_becomeseller'] = 'Syarat Menjadi Penjual';

$lang['public_footer_title_payment'] = 'Pembayaran';

// breadcrum
$lang['bc_shopping'] = 'Belanja';

// kategori product filter
$lang['filter_type'] = 'Tipe Produk';
$lang['filter_location'] = 'Lokasi Seller';
$lang['filter_price'] = 'Harga Produk';
$lang['filter_brand'] = 'Merek Produk';
$lang['filter_category'] = 'Produk Category';
$lang['filter_all_loc'] = 'Semua Provinsi';

// auth buyer
$lang['auth_forgot'] = 'Lupa Password';
$lang['auth_regis'] = 'Daftar Akun';
$lang['auth_username_req'] = 'Username Wajib Diisi !';
$lang['auth_password_req'] = 'Password Wajib Diisi !';
$lang['auth_all_req'] = 'Username dan Password Wajib Diisi !';
$lang['auth_login_success'] = 'Anda Berhasil Login !';
$lang['auth_login_wrong'] = 'Username atau Password Yang Anda Masukkan Salah !';
$lang['auth_login_notseller'] = 'Login Gagal ! <br/> Akun Anda Tidak Memiliki Hak Akses Untuk Login Sebagai Penjual !';
$lang['auth_login_notbuyer'] = 'Login Gagal ! <br/> Akun Anda Tidak Memiliki Hak Akses Untuk Login Sebagai Pembeli !';
$lang['auth_login_notfound'] = 'Akun tidak ditemukan !';



// whislist
$lang['wl_product'] = 'Nama Produk';
$lang['wl_price'] = 'Harga';
$lang['wl_detail'] = 'Detail Produk';
// cart
$lang['cart_title'] = 'Keranjang';
$lang['cart_product'] = 'Nama Produk';
$lang['cart_price'] = 'Harga';
$lang['cart_qty'] = 'Jumlah Barang';
$lang['cart_total'] = 'Total';
$lang['cart_continue'] = 'Lanjutkan Belanja';

// validasi
$lang['val_must_login'] = 'Anda harus login terlebih dahulu !';
$lang['val_must_login_buyer'] = 'Produk gagal ditambahkan kedalam wishlist ! <br/> Anda harus login dengan akun pembeli terlebih dahulu !';
$lang['val_buyer_account_only'] = 'Produk gagal ditambahkan kedalam wishlist ! <br/> Fitur wishlist hanya untuk akun pembeli !';
$lang['val_wl_outstock'] = 'Produk Gagal Ditambahkan Kedalam Wishlist ! <br/> Stock Produk Telah Habis !';
$lang['val_wl_outlimit'] = 'Produk gagal ditambahkan kedalam wishlist ! <br/> Stock produk tidak mencukupi permintaan  !';
$lang['val_cart_outstock'] = 'Produk gagal ditambahkan kedalam keranjang ! <br/> Produk yang diwishlist tidak ready stock !';
$lang['val_wl_allready'] = 'Produk Gagal Ditambahkan Kedalam Wishlist ! <br/> Produk Yang Anda Pilih Sudah Ditambahkan Kedalam Wishlist !';
$lang['val_cart_allready'] = 'Produk Gagal Ditambahkan Kedalam Keranjang ! <br/> Produk Yang Anda Pilih Sudah Ditambahkan Kedalam Keranjang!';
$lang['val_succes_added'] = 'Produk Berhasil Ditambahkan Kedalam Wishlist !';
$lang['val_succes_added_to_cart'] = 'Produk Berhasil Ditambahkan Kedalam Keranjang !';
$lang['val_error_added_to_cart'] = 'Produk gagal ditambahkan kedalam keranjang ! </> Terdapat error pada sistem';
$lang['val_success_added'] = 'Produk Berhasil Ditambahkan Kedalam Wishlist !';
$lang['val_succes_load_wl'] = 'Wishlist produk berhasil ditampilkan !';
$lang['val_wl_noproduct'] = 'Tidak ada produk dalam wishlist !';
$lang['val_wl_add_unfind'] = 'Produk Gagal Ditambahkan Kedalam Wishlist ! <br/> Produk tidak ditemukan !';
$lang['val_cart_add_unfind'] = 'Produk Gagal Ditambahkan Kedalam Keranjang ! <br/> Produk tidak ditemukan !';
$lang['val_cart_fail_b2b'] = 'Produk gagal ditambahkan ! <br/> Produk B2B minimal pembelian 10 produk !';
$lang['val_wl_fail_del'] = 'Produk gagal dihapus dari wishlist !';
$lang['val_wl_success_del'] = 'Produk berhasil dihapus dari wishlist !';
$lang['val_wl_success_del_empty'] = 'Produk berhasil dihapus dari wishlist !
<br/> Wishlist produk anda sekarang kosong';
