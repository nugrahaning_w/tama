<?php
//halaman public

//menu content public
$lang['public_header_menu_shopping'] = 'Shopping';
$lang['public_header_menu_photo'] = 'Photo';
$lang['public_header_menu_livizobusiness'] = 'Livizo Expo';
$lang['public_header_menu_news'] = 'News';
$lang['public_header_currency'] = 'Currency';
$lang['public_header_language'] = 'Language';
$lang['public_header_searchproduct'] = 'Search products ...';
$lang['public_main_title_brands'] = 'Brands';

//main content public
$lang['public_main_title_icon'] = 'The Solution for All Your Furniture and Interior Needs';
$lang['public_main_title_icon_shopping'] = 'Shopping';
$lang['public_main_title_icon_professional'] = 'Professional';
$lang['public_main_title_icon_expo'] = 'Virtual Expo';
$lang['public_main_icon_shopping'] = 'Decorate your home with our unique design products';
$lang['public_main_icon_professional'] = 'Find the professional service for your personal and business needs';
$lang['public_main_icon_expo'] = 'Enjoy the virtual furniture expo at the comfort of your home';

$lang['public_main_title_category'] = 'Product Category';
$lang['public_main_category_living_room'] = 'Living Room';
$lang['public_main_category_dining_room'] = 'Dining Room';
$lang['public_main_category_family_room'] = 'Family Room';
$lang['public_main_category_bed_room'] = 'Bed Room';
$lang['public_main_category_home_office'] = 'Home Office';
$lang['public_main_category_home_decor'] = 'Home Decor';
$lang['public_main_category_outdoor'] = 'Outdoor';
$lang['public_main_category_kitchen'] = 'Kitchen';
$lang['public_main_title_newproduct'] = 'New Product';
$lang['public_main_title_promoproduct'] = 'Product Promo';
$lang['public_main_title_businessproduct'] = 'Business Product';

$lang['public_main_title_professional'] = 'Meet with Professional';
$lang['public_main_professional'] = 'Professional design and home decoration';
$lang['public_main_professional_shopnow'] = 'Read more';

$lang['public_main_title_ife'] = 'International Furniture Online Exhibition 2020';
$lang['public_main_date_ife'] = '15 November – 05 Desember 2020';
$lang['public_main_ife'] = 'Start your virtual furniture expo journey with IFE 2020';
$lang['public_main_ife_register'] = 'REGISTER NOW';

$lang['public_main_title_product'] = 'Choose Products according to your style and taste';
$lang['public_main_product'] = 'Provides many choices of styles from famous brands';
$lang['public_main_product_shopnow'] = 'Shop Now';

$lang['public_main_title_designchoices'] = 'Design Choices';
$lang['public_main_designchoices_findallideas'] = 'Find All Ideas';

$lang['public_footer_title_help'] = 'Need Help ?';

$lang['public_footer_title_navigation'] = 'Navigation';
$lang['public_footer_list_home'] = 'Home';
$lang['public_footer_list_shopping'] = 'Shopping';
$lang['public_footer_list_photo'] = 'Photo';
$lang['public_footer_list_livizobusiness'] = 'Livizo Business';
$lang['public_footer_list_news'] = 'News';

$lang['public_footer_title_aboutus'] = 'About us';
$lang['public_footer_list_livizoteam'] = 'Livizo Team';
$lang['public_footer_list_career'] = 'Career';
$lang['public_footer_list_policyandprivacy'] = 'Policy And Privacy';
$lang['public_footer_list_contactus'] = 'Contact us';

$lang['public_footer_title_sellernprofessional'] = 'Seller & Professional';
$lang['public_footer_list_regisselleraccount'] = 'Register Seller Account';
$lang['public_footer_list_sellerlogin'] = 'Seller Login';
$lang['public_footer_list_becomeseller'] = 'Requirements To Be A Seller';

$lang['public_footer_title_payment'] = 'Payment';


// breadcrum
$lang['bc_shopping'] = 'Shopping';

// kategori product filter
$lang['filter_type'] = 'Product Type';
$lang['filter_location'] = 'Seller Location';
$lang['filter_price'] = 'Price Product';
$lang['filter_brand'] = 'Brand Product';
$lang['filter_category'] = 'Product Category';
$lang['filter_all_loc'] = 'All Location';

// auth buyer
$lang['auth_forgot'] = 'Forgot Password';
$lang['auth_regis'] = 'Registration Account';
$lang['auth_username_req'] = 'Username is Required !';
$lang['auth_password_req'] = 'Password is Required !';
$lang['auth_all_req'] = 'Username and Password are Required !';
$lang['auth_login_success'] = 'You have successfully logged in!';
$lang['auth_login_wrong'] = 'Incorrect Password !';
$lang['auth_login_notseller'] = 'Login Failed! <br/> Your account does not have seller access!';
$lang['auth_login_notbuyer'] = 'Login Failed! <br/> Your account does not have buyer access!';
$lang['auth_login_notfound'] = 'Account not found !';

// whislist
$lang['wl_product'] = 'Product Name';
$lang['wl_price'] = 'Price';
$lang['wl_detail'] = 'View Detail';
// cart
$lang['cart_title'] = 'Cart';
$lang['cart_product'] = 'Product Name';
$lang['cart_price'] = 'Price';
$lang['cart_qty'] = 'Quantity';
$lang['cart_total'] = 'Total';
$lang['cart_continue'] = 'Continue Shopping';

// msg wishlist
$lang['val_must_login'] = 'You must login first !';
$lang['val_must_login_buyer'] = 'Product failed to add to wishlist! <br/> You must log in with the buyer`s account first!';
$lang['val_buyer_account_only'] = 'Product failed to add to wishlist ! <br/> Wishlist feature is only for buyer`s accounts !';
$lang['val_wl_outlimit'] = 'Product failed to add to wishlist! <br/> Product stock is insufficient!';
$lang['val_succes_added'] = '
Product Successfully Added to Wishlist!';
$lang['val_succes_added_to_cart'] = 'Product Successfully Added to Cart!';
$lang['val_wl_allready'] = ' Failed Product Added to Wishlist! The Product You Choose Has Been Added to the Wishlist!';
$lang['val_wl_outstock'] = 'Failed Product Added to Wishlist! <br/> Product Stock Has Run Out!';
$lang['val_success_added'] = 'Product Successfully Added to Wishlist !';
$lang['val_succes_load_wl'] = 'Product wishlist successfully displayed!';
$lang['val_wl_noproduct'] = 'No products on wishlist !';
$lang['val_cart_outstock'] = 'Product failed to add to Cart! <br/> The product is not ready stock!';
$lang['val_wl_add_unfind'] = 'Failed Product Added to Wishlist! <br/> Product not found!';
$lang['val_cart_add_unfind'] = 'Failed Product Added to Cart! <br/> Product not found!';
$lang['val_cart_allready'] = 'Product Fails to Add to Cart! <br/> The Product You Choose Has Been Added to the Cart!';
$lang['val_cart_fail_b2b'] = 'Product failed to add ! <br/> B2B products have a minimum purchase of 10 products!';
$lang['val_error_added_to_cart'] = 'Product failed to add to Cart! </> There is an error in the system';
$lang['val_wl_fail_del'] = 'Product failed to delete from wishlist!';
$lang['val_wl_success_del'] = 'The product was successfully deleted from the wishlist!';
$lang['val_wl_success_del_empty'] = 'The product was successfully deleted from the wishlist!
<br/> Your Wishlist is now empty';
