<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Backend_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Product_model', 'product');
		$this->load->helper('url');
		$this->load->library('pagination');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		$this->show(); 
	}

	function show() {
		$data = [];
		$data['breadcrumb'] = [];
		$this->template->title( 'PT. Multi Prima Sejahtera' );
		$this->template->content( 'products/admin_view');
		$this->template->show(THEMES_ADMIN . 'index');
	}
	public function loadRecord($rowno = 0)
	{

		$rowperpage = 16;

		// Untuk menampilkan data mulai dari baris ke-$rowno
		if ($rowno != 0) {
			$rowno = ($rowno - 1) * $rowperpage;
		}

		// All records count
		$allcount = $this->product->getrecordCount();

		// Get records
		$dataku = $this->product->getData($rowno, $rowperpage);
		foreach ($dataku as $pr) {
			$product_id = $pr['product_id'];
			if (!empty($pr['product_img'])){
				$img = $pr['product_img'];
			}else{
				$img = 'default.jpg';
			}
			$product_list[] = array(
				'product_id' => $product_id,
				'product_name' => $pr['product_name'],
				'galery_product_image' => $img
			);
		}
		// disp($product_list);
		// die;

		// Pagination Configuration
		$config['base_url'] = base_url('product/professional');
		$config['total_rows'] = $allcount;
		$config['per_page'] = $rowperpage;
		$config['use_page_numbers'] = TRUE;
		// $config["uri_segment"] = 3;  // uri parameter
		// $choice = $config["total_rows"] / $config["per_page"];
		// $config["num_links"] = floor($choice);
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Previous';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Berikutnya</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		// Initialize
		$this->pagination->initialize($config);

		// Initialize $data Array
		$data['pagination'] = $this->pagination->create_links();
		$data['result'] = $product_list;
		$data['row'] = $rowno;

		echo json_encode($data);
	}
	function form() {
		// disp('halo');
		$data['id'] = $_GET['id'];
		if (!empty($_GET['id'])) {
			$params = [
				'table' => 'sys_product',
				'where' => [
					'product_id' => $data['id']
				],
			];
			$data['product'] = $this->crud->get_data( $params )->row_array();
		}else{
			$data['product'] = [];
		}
		// disp($data['product']);
		// die;
		$this->load->view( 'products/Form_data', $data);
	}
}