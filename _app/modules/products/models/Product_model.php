<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{

    // Fetch records
    public function getData($rowno, $rowperpage)
    {
        $this->db->select('product_id, product_name, product_img');
        $this->db->from('sys_product');
        $this->db->order_by('product_id', 'desc');
        $this->db->group_by('product_id');
        $this->db->limit($rowperpage, $rowno);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    // Select total records
    public function getrecordCount()
    {
        $this->db->select('*');
        $this->db->from('sys_product');
         $this->db->order_by('product_id', 'desc');
        $this->db->group_by('product_id');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }
}