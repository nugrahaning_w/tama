<div class="row">
	<div class="col-12">
		<div class="card m-b-30">
			<div class="card-body shadow-box">
				<ul class="nav nav-pills">
					<li class="nav-item">
						<button id = "add_product" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Tambah Produk</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<div id="list-product" class="row list-produk-seller"></div>
				<div style='margin-top: 10px;' id='pagination'></div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="productModal">
	<div class="modal-dialog modal-xl">
		<div id="content" class="modal-content">
		</div>
	</div>
</div>

<div class="modal fade" id="modal_delete_m_n"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h4 class="modal-title">Are you sure to Delete this <span class="grt"></span> ?</h4>
			</div>
			
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<span id="preloader-delete"></span>
				</br>
					<button type="button" class="btn btn-danger" id="delete_link_m_n" >Delete</button>
				<button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link">Cancel</button>
				
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		// Detect pagination click
		$('#pagination').on('click', 'a', function(e) {
			e.preventDefault();
			var pageno = $(this).attr('data-ci-pagination-page');
			loadPagination(pageno);
			console.log('pagination = '.pageno);
		});

		loadPagination(0);

		// Load pagination
		function loadPagination(pagno) {
			console.log("loading.....");
			$.ajax({
				url: '<?php echo base_url(); ?>products/admin/loadRecord/' + pagno,
				type: 'get',
				dataType: 'json',
				success: function(response) {
					$('#pagination').html(response.pagination);
					list_produk(response.result);
				}
			});
		}

		//Menampilkan list data produk seller
		function list_produk(result) {
			let html = '';
			$.each(result, function(key, value) {
				html += `
				<div class="col-md-3 col-sm-3 col-xs-12 ">
					<div class="product product-grid">
						<div class="product-media">
							
							<div class="product-thumbnail">
								<a href="javascript:void(0);" title="" class="product-quick-view" >
									<img src="<?php echo IMAGES; ?>product/${value.galery_product_image}" alt="" class="current image-holder-270">
								</a>
							</div>
							<div class="product-hover">
								<div class="product-actions">
									<a href="javascript:void(0);" class="awe-button product-quick-view-ubah" data-id="${value.product_id}" data-toggle="tooltip" title="Quickview">
										Ubah
									</a>
									<a href="javascript:void(0);" class="awe-button product-quick-view-hapus" onclick="modalOnHapus(${value.product_id})" data-toggle="tooltip" title="Quickview">
										Hapus
									</a>
								</div>
							</div>
						</div>
						<div class="product-body">
							<h2 class="product-name">
								<a href="#" title="${value.product_name}">${value.product_name}</a>
							</h2>
						</div>
					</div>
				</div>`;
				$('#list-product').html(html);
			});

		}
		// list_produk();
		$('#list-product').on('click', '.product-quick-view-ubah', function() {
			let id = $(this).attr('data-id');
			$.ajax({
				url: '<?php echo base_url('product/professional/get_form/'); ?>' + id,
				method: "GET",
				dataType: "HTML",
				success: function(res) {
					$('#productModal-content').html(res);
					$('#productModal').modal('show');
				},
				error: function(xhr) {}
			});
		});

		$('button.hapus_product').on('click', function() {
			let form_Notice = $('div.form-notice-hapus-produk');
			const id = $(this).data('id');
			$.ajax({
				url: '<?php echo base_url('product/admin/hapus_product'); ?>',
				data: {
					product_id: id
				},
				method: 'POST',
				dataType: "JSON",
				success: function(respons) {
					let status = respons.status_code;
					if (status == '200') {
						form_Notice.html(`<div class="alert alert-success" role="alert">${respons.message}</div>`);
						if (respons.url != '') {
							location.href = respons.url;
						}
					} else {
						form_Notice.html(`<div class="alert alert-danger" role="alert">${respons.message}</div>`);
					}
				}
			});
		});
	});

	function toSlug(Text) {
		return Text
			.toLowerCase()
			.replace(/ /g, '-')
			.replace(/[^\w-]+/g, '');
	}

	$(document).on( "click", "#add_product", function(){
	$.ajax	({
			url: "<?php echo base_url()?>products/Admin/form?id=0",
			type: "GET",
			dataType: "html",
			success: function( res ){
				$("#content").html( res );
				$("#productModal").modal( "show" );
			},
			error: function(){
				alert("error");
			}
			});
	});
	$(document).on( "click", "a.product-quick-view-ubah", function(){
		let id = $(this).data('id');
	$.ajax	({
			url: "<?php echo base_url()?>products/Admin/form?id="+id,
			type: "GET",
			dataType: "html",
			success: function( res ){
				$("#content").html( res );
				$("#productModal").modal( "show" );
			},
			error: function(){
				alert("error");
			}
			});
	});

	//REQUEST AJAX PRODUK PILIHAN
	function modalOnHapus(data = '') {
		$('#myModalHapus').modal();
		let product_id = '';
		$("button.hapus_product").attr("data-id", data);
	}
</script>