
<div class="modal-header">
	<?php if (!empty($id)){
			echo "<h3 class='modal-title'>Edit mechine</h3>";
		}else{
			echo "<h3 class='modal-title'>Tambah mechine</h3>";	
		}
	?>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
	<div class="row col-12">
		<form id="form-admin" class="col-12">
        <input type="hidden" name="type" id="type" value=1>
			<div class="row col-12 form-group">

				<input type="hidden" name="id" id = "id" value="<?php if (!empty($id)){
						echo $id;
				}?>">
                
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="card">
				<div class="card-body shadow-box">
					<form class="livizo" method="post" action="<?= base_url('mechine/mechine_tambah'); ?>">
						<h4 class="header-title">Tambah Mesin</h4>
						<p class="sub-title"></p>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>Nama Mesin</label>
									<input type="text" placeholder="" data-mask="999.999.999.9999" class="form-control mechine_name" name="mechine_name" value = "<?php if (!empty($mechine)){echo $mechine['mechine_name'];} else { echo '';}?>" required>
								</div>
							</div>
							
							<div class="col-12">
								<div class="form-group">
									<label>Upload Gambar</label>
									<div>
									<input type="file" name="" id="">
									</div>
								</div>
							</div>    
							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
			</div>

			</div>
		</form> 
	</div>
</div>
<div class="modal-footer">
	<button type="button" id="saveAdmin" class="btn btn-info">Simpan</button>
	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
<script>
	$(document).ready(function() {
		$('#saveAdmin').on( 'click', function(){
            $.ajax({
				url: "<?php echo base_url();?>users/Admin/act_save",
				type: "POST",
				data: $('#form-admin').serialize(),
				dataType: "json",
				success: function( res ){
                    console.log(res);
					if ( res.code == '200' ){
						$('#modalUmum').modal('hide');
                        location.reload()
					}
                    if ( res.code == '400' ) {
                        alert( res.message );
                    }
				},
				error: function(){
					alert("error");
				}
			});
		});
	});
</script>