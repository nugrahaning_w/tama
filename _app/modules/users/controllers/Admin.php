<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->show(); 
	}
	
	function show(){
		$data = [];
		$this->template->header_script( '
			<link href="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
			<link href="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">' );
		$this->template->footer_script( '
			<script src="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
			<script src="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
			<script>
				$(document).ready(function() {
				
					$("#table_admin").DataTable({
						ajax: "' . base_url() . 'users/admin/get_data_admin",
						columns: [
							{ "data": "id", "defaultContent": "0" },
							{ "data": "username", "defaultContent": "none" },
							{ "data": "created_at", "defaultContent": "none" },
							{ "data": "last_login", "defaultContent": "none" },
							{ "data": "action", "defaultContent": "none" },
						],
						pageLength: 5,
						responsive: true,
						dom: \'<"top text-left"fl>rt<"bottom"ip><"clear">\',
						loadingRecords: "Mengambil Data ...",
						language: {
							emptyTable: "Tidak ada data",
							zeroRecords: "No records to display"
						},
					});

					$("#table_supervisor").DataTable({
						ajax: "' . base_url() . 'users/admin/get_data_spv",
						columns: [
							{ "data": "id", "defaultContent": "0" },
							{ "data": "username", "defaultContent": "none" },
							{ "data": "created_at", "defaultContent": "none" },
							{ "data": "last_login", "defaultContent": "none" },
							{ "data": "action", "defaultContent": "none" },
						],
						pageLength: 5,
						responsive: true,
						dom: \'<"top text-left"fl>rt<"bottom"ip><"clear">\',
						loadingRecords: "Mengambil Data ...",
						language: {
							emptyTable: "Tidak ada data",
							zeroRecords: "No records to display"
						},
					});

					$("#table_fm").DataTable({
						ajax: "' . base_url() . 'users/admin/get_data_fm",
						columns: [
							{ "data": "id", "defaultContent": "0" },
							{ "data": "username", "defaultContent": "none" },
							{ "data": "created_at", "defaultContent": "none" },
							{ "data": "last_login", "defaultContent": "none" },
							{ "data": "action", "defaultContent": "none" },
						],
						pageLength: 5,
						responsive: true,
						dom: \'<"top text-left"fl>rt<"bottom"ip><"clear">\',
						loadingRecords: "Mengambil Data ...",
						language: {
							emptyTable: "Tidak ada data",
							zeroRecords: "No records to display"
						},
					});

					
				});
			</script>
			' );
		$data['breadcrumb'] = [];
		$this->template->title( 'PT. Multi Prima Sejahtera' );
		$this->template->content( 'users/Admin_view', $data );
		$this->template->show( 'themes/admin/index' );
	}
	
	function get_data_admin(){
		$params = [
			'table' => 'core_user_account a',
			'select' => 'a.user_account_id, a.user_account_username, a.user_account_created_at, a.user_account_last_login, a.user_account_is_login',
			'where' => [
				'a.user_account_type_id' => 1
			]
		];
		$query = $this->crud->get_data( $params );
		$arr_data = [];
		if ( $query->num_rows() > 0 ) {
			foreach ($query->result() as $key => $value) {
				$arr_data['data'][] = [
					'id' => $value->user_account_id,
					'username' => $value->user_account_username,
					'created_at' => $value->user_account_created_at,
					'last_login' => $value->user_account_last_login,
					'action' => '<button class="btn btn-warning btn-sm edit_admin" data-id = '.$value->user_account_id.' data-type = 1><i class="fa fa-edit"></i>&nbsp;&nbsp;Ubah</button>
					<button class="btn btn-danger btn-sm delete_admin" id = '.$value->user_account_id.'><i class="fa fa-trash" ></i>&nbsp;&nbsp;Hapus</button>'
				];
			}
		} else {
			$arr_data['data'] = [];
		}
		die( json_encode( $arr_data ) );
	}

	function get_data_spv(){
		$params = [
			'table' => 'core_user_account a',
			'select' => 'a.user_account_id, a.user_account_username, a.user_account_created_at, a.user_account_last_login, a.user_account_is_login',
			'where' => [
				'a.user_account_type_id' => 2
			]
		];
		$query = $this->crud->get_data( $params );
		$arr_data = [];
		if ( $query->num_rows() > 0 ) {
			foreach ($query->result() as $key => $value) {
				$arr_data['data'][] = [
					'id' => $value->user_account_id,
					'username' => $value->user_account_username,
					'created_at' => $value->user_account_created_at,
					'last_login' => $value->user_account_last_login,
					'action' => '<button class="btn btn-warning btn-sm edit_spv" data-id = '.$value->user_account_id.' data-type = 2><i class="fa fa-edit"></i>&nbsp;&nbsp;Ubah</button>
					<button class="btn btn-danger btn-sm delete_spv" id = '.$value->user_account_id.'><i class="fa fa-trash" ></i>&nbsp;&nbsp;Hapus</button>'
				];
			}
		} else {
			$arr_data['data'] = [];
		}
		die( json_encode( $arr_data ) );
	}

	function get_data_fm(){
		$params = [
			'table' => 'core_user_account a',
			'select' => 'a.user_account_id, a.user_account_username, a.user_account_created_at, a.user_account_last_login, a.user_account_is_login',
			'where' => [
				'a.user_account_type_id' => 3
			]
		];
		$query = $this->crud->get_data( $params );
		$arr_data = [];
		if ( $query->num_rows() > 0 ) {
			foreach ($query->result() as $key => $value) {
				$arr_data['data'][] = [
					'id' => $value->user_account_id,
					'username' => $value->user_account_username,
					'created_at' => $value->user_account_created_at,
					'last_login' => $value->user_account_last_login,
					'action' => '<button class="btn btn-warning btn-sm edit_fm" data-id = '.$value->user_account_id.' data-type = 3><i class="fa fa-edit"></i>&nbsp;&nbsp;Ubah</button>
					<button class="btn btn-danger btn-sm delete_fm" id = '.$value->user_account_id.'><i class="fa fa-trash" ></i>&nbsp;&nbsp;Hapus</button>'
				];
			}
		} else {
			$arr_data['data'] = [];
		}
		die( json_encode( $arr_data ) );
	}

	function form() {
		$data = [];
		$data['type'] = $_GET['type'];
		$data['id'] = $_GET['id'];
		if (!empty($_GET['id'])) {
			$params = [
				'table' => 'core_user_account',
				'where' => [
					'user_account_id' => $data['id'],
					'user_account_type_id' => $data['type']
				],
			];
			$data_user = $this->crud->get_data( $params )->row();
			$data['username'] = $data_user->user_account_username;
		}else{
			$data['username'] = "";
		}
		if ($_GET['type'] == 1 ) {
			$this->load->view( 'users/Admin_add', $data);
		}elseif ($_GET['type'] == 2 ){
			$this->load->view( 'users/Spv_add', $data);
		}
		elseif ($_GET['type'] == 3 ){
			$this->load->view( 'users/Foreman_add', $data);
		}
	}

	function act_save(){
		$in = $this->input->post();
		$type = $in["type"];
		$id = $in["id"];
		$username = $in["username"];
		$pass = $in["password"];
		$datetime = date( "Y-m-d H:i:s" );
		if(!empty($id)){
			if (!empty($pass)) {
				$where = array(
					'user_account_id' => $id,
					'user_account_type_id' => $type
				);
				$data = array(
					'user_account_username' => $username,
					'user_account_password' => password_hash( $in['password'], PASSWORD_DEFAULT),
					'user_account_created_at' => $datetime
				);
				$table = 'core_user_account';
				$update_account = $this->crud->save_data($table, $data, $where);
			}else{
				$where = array(
					'user_account_id' => $id,
					'user_account_type_id' => $type
				);
				$data = array(
					'user_account_username' => $username,
					'user_account_created_at' => $datetime
				);
				$table = 'core_user_account';
				$update_account = $this->crud->save_data($table, $data, $where);
			}
			if ($update_account){
				$res = [
					'code' => '200',
					'message' => 'Data admin berhasil disimpan !',
				];
			}else{
				$res = [
					'code' => '400',
					'message' => 'Data admin gagal disimpan !',
				];
			}
		}else{
			if (!empty($in['username'])) {
				if(!empty($in['password'])){
					$uname_is_available =  $this->cek_username( $in['username'], '0' );
					if ( $uname_is_available == false ) {
						$message .= 'Nama Pengguna Sudah Digunakan.';
						$res = [
							'code' => '400',
							'message' => $message
						];
						die( json_encode( $res ) );
					}
					$save_data_account = [
						'user_account_username' => $in['username'],
						'user_account_password' => password_hash( $in['password'], PASSWORD_DEFAULT),
						'user_account_type_id' => $type,
						'user_account_created_at' => $datetime,
					];
					$account_id = $this->crud->save_data( 'core_user_account', $save_data_account );
	
					if ( $account_id ) {
						$res = [
							'code' => '200',
							'message' => 'Data admin berhasil disimpan !',
						];
					} else {
						$res = [
							'code' => '400',
							'message' => 'Data admin Gagal disimpan !',
						];
					}
				}else{
					$res = [
						'code' => '400',
						'message' => 'Password tidak boleh kosong !',
					];
				}
			}else{
				$res = [
					'code' => '400',
					'message' => 'Username tidak boleh kosong !',
				];
	
			}
		}
		die( json_encode( $res ) );
	}

	function delete(){
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$column = array(
			'user_account_type_id' => $type,
			'user_account_id' => $id
		);
		$table = "core_user_account";
		$query = $this->crud->delete_data($table, $column);
		if ( $query ) {
			$respons = [
				'code' => 200,
				'msg' => 'Sukses dihapus !',
				'data' => '',
				'url' => ''
			];	
		} else {
			$respons = [
				'code' => 400,
				'msg' => 'Gagal dihapus !',
				'data' => '',
				'url' => ''
			];	
		}
		die( json_encode( $respons ) );
	}

	function cek_username( $uname, $is_json = '1' ){
		$uname = $this->input->post('username');
		$params = [
			'table' => 'core_user_account',
			'where' => [
				'user_account_username' => $uname
			],
		];

		$cek_uname = $this->crud->get_data( $params );

		if ( $is_json ) {
			$res = [
				'code' => 200,
				'message' => 'Nama Pengguna Tersedia.'
			];
			if ( $cek_uname->num_rows() > 0 ) {
				$res = [
					'code' => 400,
					'message' => 'Nama Pengguna Sudah digunakan.'
				];	
			}
			die( json_encode( $res ) );
		} else {
			if ( $cek_uname->num_rows() > 0 ) {
				return false;
			} else {
				return true; 
			}
		}
	}

	}
