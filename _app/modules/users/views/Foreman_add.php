<div class="modal-header">
	
	<?php if (!empty($username)){
			echo "<h3 class='modal-title'>Edit Foreman</h3>";
		}else{
			echo "<h3 class='modal-title'>Tambah Foreman</h3>";	
		}
	?>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
	<div class="row col-12">
		<form id="form-admin" class="col-12">
        <input type="hidden" name="type" id="type" value=3>
			<div class="row col-12 form-group">
				<input type="hidden" name="id" id = "id" value="<?php if (!empty($id)){
						echo $id;
					}?>">
				<div class="col-6">
					<label>Username</label>
					<div class="col-12">
						<input name="username" id="username" class="form-control cek" value = "<?php if (!empty($username)){
						echo $username;
						}?>" required>
					</div>
				</div>
				<div class="col-6">
					<label>Password</label>
					<div class="col-12">
						<input name="password" class="form-control">
					</div>
					<?php if (!empty($username)){
						echo "<p>*Kosongkan password apabila ingin menggunakan password lama</p>";
					}
					?>
				</div>
			</div>

			</div>
		</form> 
	</div>
</div>
<div class="modal-footer">
	<button type="button" id="saveforeman" class="btn btn-info">Simpan</button>
	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
<script>
	$(document).ready(function() {
		$('#saveforeman').on( 'click', function(){
            $.ajax({
				url: "<?php echo base_url();?>users/Admin/act_save",
				type: "POST",
				data: $('#form-admin').serialize(),
				dataType: "json",
				success: function( res ){
                    console.log(res);
					if ( res.code == '200' ){
						$('#modalUmum').modal('hide');
                        location.reload()
					}
                    if ( res.code == '400' ) {
                        alert( res.message );
                    }
				},
				error: function(){
					alert("error");
				}
			});
		});

		
	});
</script>