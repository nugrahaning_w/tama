<div class="col-12 from-control">
	<ul class="nav nav-tabs">
		<li class="nav-item">
		 	<a class="nav-link active" data-toggle="tab" href="#admin">Admin</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#professional">Supervisor</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#buyer">Foreman</a>
		</li>
	</ul>
	<div class="tab-content">
		<div id="admin" class="row tab-pane active">
			<div class="col-12 form-group">
				<h4>Admin</h4>
			</div>
			<div class="col-12 form-group">
				<button id="add_admin" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp;Tambah Admin</button>
			</div>
			<div class="col-12 form-group">
				<table id="table_admin" class="display" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Created at</th>
							<th>Last Login</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div id="professional" class="row  tab-pane fade">
			<div class="col-12 form-group">
				<h4>Supervisor</h4>
			</div>
			<div class="col-12 form-group">
				<button id="add_spv" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp;Tambah Supervisor</button>
			</div>
			<div class="col-12 form-group">
				<table id="table_supervisor" class="display" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Created at</th>
							<th>Last Login</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		<div id="buyer" class="row  tab-pane fade">
			<div class="col-12 form-group">
				<h4>Foreman</h4>
			</div>
			<div class="col-12 form-group">
				<button id="add_foreman" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>&nbsp;Tambah Foreman</button>
			</div>
			<div class="col-12 form-group">
				<table id="table_fm" class="display" style="width:100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Created at</th>
							<th>Last Login</th>
							<th>Aksi</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalUmum">
	<div class="modal-dialog modal-xl">
		<div id="content" class="modal-content"></div>
	</div>
</div>
<div class="modal fade" id="modal_delete_m_n"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h4 class="modal-title">Are you sure to Delete this <span class="grt"></span> ?</h4>
			</div>
			
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<span id="preloader-delete"></span>
				</br>
					<button type="button" class="btn btn-danger" id="delete_link_m_n" >Delete</button>
				<button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link">Cancel</button>
				
			</div>
		</div>
	</div>
</div>
<script>
$(document).on( "click", "#add_admin", function(){
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/form?type=1",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalUmum").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
});

$(document).on( "click", "#add_spv", function(){
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/form?type=2",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalUmum").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
});

$(document).on( "click", "#add_foreman", function(){
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/form?type=3",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalUmum").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
});


$('#modal_delete_m_n').on('hidden.bs.modal', function () {
	$('#delete_link_m_n').removeAttr('data-id')
})

$(document).on( "click", ".delete_admin", function(){
	const id = this.id
	$("#modal_delete_m_n").modal( "show" );
	$("#delete_link_m_n").attr('data-id', id)
	$("#delete_link_m_n").attr('data-type', 1)
});
$(document).on( "click", ".delete_spv", function(){
	const id = this.id
	$("#modal_delete_m_n").modal( "show" );
	$("#delete_link_m_n").attr('data-id', id)
	$("#delete_link_m_n").attr('data-type', 2)
});
$(document).on( "click", ".delete_fm", function(){
	const id = this.id
	$("#modal_delete_m_n").modal( "show" );
	$("#delete_link_m_n").attr('data-id', id)
	$("#delete_link_m_n").attr('data-type', 3)
});

$(document).on( "click", "#delete_link_m_n", function(){
	const id = $(this).attr("data-id")
	const type = $(this).attr("data-type")
	let data = {
		id : id,
		type : type
	}
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/delete",
		type: "POST",
		data: data,
		dataType: "json",
		success: function( res ){
			location.reload()
		},
		error: function(){
			alert("error");
		}
	});
})

$(document).on( "click", ".edit_admin", function(){
	const id = $(this).attr('data-id')
	const type = $(this).attr('data-type')
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/form?type="+type+"&&id="+id+"",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalUmum").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
})

$(document).on( "click", ".edit_spv", function(){
	const id = $(this).attr('data-id')
	const type = $(this).attr('data-type')
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/form?type="+type+"&&id="+id+"",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalUmum").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
})

$(document).on( "click", ".edit_fm", function(){
	const id = $(this).attr('data-id')
	const type = $(this).attr('data-type')
	$.ajax({
		url: "<?php echo base_url()?>users/Admin/form?type="+type+"&&id="+id+"",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalUmum").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
})
</script>

