<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rejected extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->show(); 
	}

	function show() {
		$data = [];
		$this->template->header_script( '
			<link href="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
			<link href="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">' );
		$this->template->footer_script( '
			<script src="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
			<script src="' . base_url() . 'assets/addons/datatables/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
			<script>
				$(document).ready(function() {
				
					$("#table_rejected").DataTable({
						ajax: "' . base_url() . 'spv/rejected/get_data_rejected",
						columns: [
							{ "data": "id", "defaultContent": "0" },
							{ "data": "product_name", "defaultContent": "none" },
							{ "data": "report_target_count", "defaultContent": "none" },
							{ "data": "report_rejected_count", "defaultContent": "none" },
							{ "data": "report_total_count", "defaultContent": "none" },
							{ "data": "report_date_input", "defaultContent": "none" },
							{ "data": "action", "defaultContent": "none" },
						],
						pageLength: 31,
						"order": [[5, "desc"]],
						responsive: true,
						dom: \'<"top text-left"fl>rt<"bottom"ip><"clear">\',
						loadingRecords: "Mengambil Data ...",
						language: {
							emptyTable: "Tidak ada data",
							zeroRecords: "No records to display"
						},
					});	
				});
			</script>
			' );
		$data['breadcrumb'] = [];
		$this->template->title( 'Supervisor' );
		$this->template->content( 'spv/List_rejected_view', $data );
		$this->template->show( 'themes/admin/index' );
	}
	function get_data_rejected(){
		$params = [
			'table' => [
				'sys_report_product' => '',
				'sys_product' => 'report_product_id = product_id'
			],
			'select' => '*'
		];
		$query = $this->crud->get_data( $params );
		$arr_data = [];
		if ( $query->num_rows() > 0 ) {
			foreach ($query->result() as $key => $value) {
				$arr_data['data'][] = [
					'id' => $value->report_id,
					'product_name' => $value->product_name,
					'report_target_count' => $value->report_target_count,
					'report_rejected_count' => $value->report_rejected_count,
					'report_total_count' => $value->report_total_count,
					'report_date_input' => $value->report_date_input,
					'action' => '<button class="btn btn-warning btn-sm detail_report" data-id = '.$value->report_id.' data-type = 1><i class="fa fa-edit"></i>&nbsp;&nbsp;Detail</button>'
				];
			}
		} else {
			$arr_data['data'] = [];
		}
		die( json_encode( $arr_data ) );
	}
	function get_detail(){
		$data['id'] = $_GET['id'];
		if (!empty($_GET['id'])) {
			$params = [
				'table' => [
					'sys_report_product' => '',
					'sys_rejected' => 'report_rejected_id = rejected_id', 
					'sys_product' => 'report_product_id = product_id'
				],
				'select' => '*',
				'where' => [
					'report_id' => $data['id']
				]
			];
			$query = $this->crud->get_data( $params ) -> row();
			$data['detail'] = $query;
		}else{
			$data['detail'] = "";
		}	
		// disp($data); die;
		$this->load->view( 'spv/Detail_view', $data);
	}
	
}