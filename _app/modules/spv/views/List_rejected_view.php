<div class="col-12 from-control">
    <div class="col-12 form-group">
            <h4>Laporan Produksi</h4>
    </div>
    <div class="col-12 form-group">
        <table id="table_rejected" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Product</th>
                    <th>Jumlah Target Produksi</th>
                    <th>Jumlah Produk Reject</th>
                    <th>Total Produksi</th>
                    <th>Tanggal Laporan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="modalDetail">
	<div class="modal-dialog modal-xl">
		<div id="content" class="modal-content"></div>
	</div>
</div>
<script>
$(document).on( "click", ".detail_report", function(){
	const id = $(this).attr('data-id')
	$.ajax({
		url: "<?php echo base_url()?>spv/rejected/get_detail?id="+id+"",
		type: "GET",
		dataType: "html",
		success: function( res ){
			$("#content").html( res );
			$("#modalDetail").modal( "show" );
		},
		error: function(){
			alert("error");
		}
	});
})
</script>

