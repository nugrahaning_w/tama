<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<form class="detail" method="post">
					<h4 class="header-title">Detail</h4>
					<p class="sub-title"></p>
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>Nama Produk</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->product_name)){
                                echo $detail->product_name;
                                }?>" required disabled>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
                            <label>Reject Karena kerusakan Wire</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->rejected_cacat_wire)){
                                echo $detail->rejected_cacat_wire;
                                }?>" required disabled>
							</div>
						</div>
						<div class="col-12">
                            <div class="form-group">
                                <label>Reject Karena kerusakan GAP</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->rejected_cacat_gap)){
                                echo $detail->rejected_cacat_gap;
                                }?>" required disabled>
							</div>
						</div>  
						<div class="col-12">
                            <div class="form-group">
                                <label>Reject Karena kerusakan Electro</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->rejected_cacat_electro)){
                                echo $detail->rejected_cacat_electro;
                                }?>" required disabled>
							</div>
						</div>
						<div class="col-12">
                            <div class="form-group">
                                <label>Reject Karena kerusakan Insulator</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->rejected_cacat_insulator)){
                                echo $detail->rejected_cacat_insulator;
                                }?>" required disabled>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<form>
                    <h4 class="header-title">Tindak Lanjut</h4>
					<p class="sub-title"></p>
					<div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Total Produk Rusak</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->report_rejected_count)){
                                echo $detail->report_rejected_count;
                                }?>" required disabled>
							</div>
						</div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Akan Di Perbaiki</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->report_repair_count)){
                                echo $detail->report_repair_count;
                                }?>" required disabled>
							</div>
						</div>
						<div class="col-12">
                            <div class="form-group">
                                <label>Akan Di Scrap</label>
                                <input class="form-control cek" value = "<?php if (!empty($detail->report_scrap_count)){
                                echo $detail->report_scrap_count;
                                }?>" required disabled>
							</div>
						</div> 
						<div class="col-12">
								<div class="form-group"> 
                                    <label for=""></label>                                                     
									<textarea id="note" name="note" placeholder="Enter text ..." style="width: 500px; height: 200px" required disabled><?php if (!empty($detail->report_note)){
                                echo $detail->report_note;
                                }?></textarea>
								</div>                               
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal fade" id="modal_alert"  data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="center">
					<h4>Ooops....<span class="grt"></span> </h4>
				</div>
				<div class="center">
						<h5>Tidak ada perubahan</h5>
					</div>
				<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
					<span id="preloader-delete"></span>
					<div class = "center">
						<button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link">Cancel</button>
					</div>
					
				</div>
			</div>
		</div>
		</div>
	</div>
		
</div>