<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Backend_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper(array('Cookie', 'String'));
	}

	public function index() {
		$this->show(); 
	}

	function show(){
		$data = [];

		$this->template->header_script( '
			<link href="' . base_url() . 'assets/addons/dataTables/DataTables-1.10.18/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
			<link href="' . base_url() . 'assets/addons/dataTables/DataTables-1.10.18/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">' );
		$this->template->footer_script( '
			<script src="' . base_url() . 'assets/addons/dataTables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
			<script src="' . base_url() . 'assets/addons/dataTables/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
			<script>
				$(document).ready(function() {
					$("#table_id").DataTable({
					    ajax: "' . base_url() . 'user_group/admin/get_data_user_group",
					    columns: [
							{ "data": "id" },
							{ "data": "parent" },
							{ "data": "type" },
							{ "data": "name" },
							{ "data": "action" },
						],
						pageLength: 5,
						responsive: true,
						dom: \'<"top text-left"fl>rt<"bottom"ip><"clear">\'
					});

					$(document).on( "click", "#buttonAdd", function(){
						$.ajax({
							url: "' . base_url() . 'user_group/admin/form",
							type: "GET",
							dataType: "html",
							success: function( res ){
								$("#content").html( res );
								$("#modalUmum").modal( "show" );
							},
							error: function(){
								alert("error");
							}
						});
					});
				});
			</script>
			' );
		$this->template->title( 'List User Group' );
		$this->template->content( 'user_group/List_user_group', $data );
		$this->template->show( 'themes/admin/marketplace/index' );
	}

	function form() {
		$data = [];
		$params_get_user_group_parent = [
			'table' => 'ref_account_type',
			'select' => 'account_type_id, account_type_parent_id, account_type_name, account_type_user',
			'where' => [
				'account_type_parent_id' => '0'
			],
			'order_by' => 'account_type_id',
		];
		$query_get_user_group_parent = $this->crud->get_data( $params_get_user_group_parent );
		$data['user_group_parent'] = $query_get_user_group_parent->result();
		$this->load->view( 'user_group/Add_user_group', $data );
	}

	function get_data_user_group(){
		$params = [
			'table' => [
				'ref_account_type a' => '',
				'ref_account_type b' => ['a.account_type_parent_id = b.account_type_id', 'left'],
			],
			'select' => 'a.account_type_id, a.account_type_name, a.account_type_user, b.account_type_name parent',
		];
		$query = $this->crud->get_data( $params );

		$arr_data = [];
		foreach ($query->result() as $key => $value) {
			$arr_data['data'][] = [
				'id' => $value->account_type_id,
				'parent' => $value->parent,
				'name' => $value->account_type_name,
				'type' => $value->account_type_user,
				'action' => '<button>aksi</button>'
			];
		}
		die( json_encode( $arr_data ) );
	}
}