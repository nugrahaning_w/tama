<div class="modal-header">
	<h3 class="modal-title">Tambah Group</h3>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
	<div class="row col-12">
		<form id="form-user-group">
			<div class="row col-12 form-group">
				<label>Tipe Pengguna</label>
				<div class="col-12">
					<select name="tipe_pengguna" class="form-control">
						<option> - Pilih Parent Tipe Pengguna - </option>
						<?php
							foreach ( $user_group_parent as $k => $v ) {
							 	echo '
						<option value="' . $v->account_type_id . '">' . $v->account_type_name . '</option>
							 	';
							 } 
						?>
					</select>
				</div>
			</div>
			<div class="row col-12 form-group">
				<label>Jenis Pengguna</label>
				<div class="col-12">
					<input name="jenis_pengguna" class="form-control">
				</div>
			</div>
		</form> 
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-info">Simpan</button>
	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>