<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends MY_Controller
{

	public function index()
	{
		$this->load->helper('Cookie');
		delete_cookie('remember_code');
		$this->session->sess_destroy();
		$user_account_id = $this->session->userdata('user_info')['user_account_id'];
		// $where = [
		// 	'history_login_account_id' 	=> $user_account_id
		// ];
		// $data = array(
		// 	'history_login_status'		=> '0'
		// );
		// $table = 'core_history_login';
		// $update_history_login = $this->crud->save_data($table, $data, $where);

		if ($update_history_login) {
			redirect('');
		} else {
			redirect('auth/login');
		}
	}
}
