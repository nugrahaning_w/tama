<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends Login_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('Cookie', 'String'));
		$this->load->model('Login_model', 'login');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		
			$this->template->title('PT Multi Prima Sejahtera');
			$this->template->show('themes/Login_v1/login');
			
	}
	public function act_login()
	{
		$this->load->helper('security');
		$now = date("Y-m-d H:i:s");
		$username = $this->input->post('username');
		$pass = $this->input->post('pass');
		$user = $this->login->check_user($username);
		// disp($now);
		if (!empty($user)) {
			if (password_verify($pass, $user->user_account_password)) {
				$user_type_id = $user->user_account_type_id;
				$user_info = [
					'user_info' => [
						'user_username' => $user->user_account_username,
						'user_account_id' => $user->user_account_id,
						'user_type' => $user->account_type_name,
						'user_last_login' => $now,
						'user_is_login' => 1,
					]
				];
				$this->session->set_userdata($user_info);
				$this->update_history_login($user->user_account_id);
				switch ($user_type_id) {
				case 1:
					$respons = [
						'code' => 200,
						'msg' => 'Login Sukses !',
						'data' => '',
						'url' => base_url('admin/dashboard')
					];	
				break;
				case 2:
					$respons = [
						'code' => 200,
						'msg' => 'Login Sukses !',
						'data' => '',
						'url' => base_url('spv/dashboard')
					];
				break;
				case 3:
					$respons = [
						'code' => 200,
						'msg' => 'Login Sukses !',
						'data' => '',
						'url' => base_url('foreman/dashboard')
					];
				break;
				}
			}else{
				$respons = [
					'code' => 404,
					'msg' => 'Password yang anda masukkan salah !',
					'data' => "FAIL",
					'url' => ''
				];
			}
		}else{
			$respons = [
				'code' => 404,
				'msg' => 'Username tidak ditemukan !',
				'data' => "FAIL",
				'url' => ''
			];
		}
		echo json_encode($respons);
	}

	function get_member($username)
	{
		$this->db->select('user_account_id, user_account_status, user_account_type_account_id, user_account_password, user_account_username, user_account_email, user_account_last_login_datetime, user_profile_picture, account_type_parent_id, company_id, company_logo, user_account_no_hp, user_profile_first_name, user_profile_last_name')
			->from('core_user_account')
			->join('core_user_profile', 'user_account_id = user_profile_id', 'LEFT')
			->join('ref_account_type', 'user_account_type_account_id = account_type_id', 'LEFT')
			->join('sys_company', 'user_account_id = company_produsen_id', 'LEFT')
			// ->where("(`a.user_account_username`='$username' OR `a.user_account_email`='$username')", NULL, FALSE);
			->group_start()
				->where('user_account_username',$username)
				->or_where('user_account_email',$username)
			->group_end();
		$res = $this->db->get();
	
				// disp($this->db->last_query());
				// disp($res->row());
		return  $res;
	}

	function update_history_login($user_account_id)
	{
		$now = date("Y-m-d H:i:s");
		$data = array(
			'user_account_last_login' 	=> $now
		);
		$where = array(
			'user_account_id' 	=> $user_account_id
		);
		$table = 'core_user_account';
		$this->crud->save_data($table, $data, $where);
	}
}
