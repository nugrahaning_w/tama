<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	function get_user($username = '')
	{
		$this->db->select('*')
			->from('core_user_account')
			->join('core_user_previlage b', 'a.user_account_id=b.user_previlage_account_id')
			->join('core_user_group c', 'b.user_previlage_user_group_id=c.user_group_id')
			->join('core_user_role d', 'c.user_group_id=d.user_role_user_group_id')
			->join('core_menu e', 'd.user_role_menu_id=e.menu_id')
			->join('core_menu_action f', 'f.menu_action_id=d.user_role_menu_action')
			->join('ref_account_type g', 'g.account_type_id=a.user_account_type_account_id')
			->where('a.user_account_username', $username);
		return $user = $this->db->get()->row_array();
	}

	function get_menu($username)
	{
		$this->db->select('*')
			->from('core_menu')
			->join('core_user_previlage b', 'a.user_account_id=b.user_previlage_account_id')
			->join('core_user_group c', 'b.user_previlage_user_group_id=c.user_group_id')
			->join('core_user_role d', 'c.user_group_id=d.user_role_user_group_id')
			->join('core_menu e', 'd.user_role_menu_id=e.menu_id')
			->join('core_menu_action f', 'f.menu_action_id=d.user_role_menu_action')
			->join('ref_account_type g', 'g.account_type_id=a.user_account_type_account_id')
			->where('a.user_account_username', $username);
		return $user = $this->db->get()->row_array();
		$params = [
			'table' => 'core_menu',
			'select' => 'menu_id, menu_parent_menu_id, menu_name, menu_slug, menu_url, menu_description, menu_image, menu_class, menu_sort',
			'where' => ['menu_is_active' => '1']
		];
		$query_menu = get_data($params);
	}

	function get_by_cookie($cookie)
	{
		$this->db->where('cookie', $cookie);
		return $this->db->get("core_user_account");
	}

	function check_user($username){
		$this->db->select('a.user_account_id, a.user_account_username, a.user_account_type_id, a.user_account_password, b.account_type_name');
		$this->db->from('core_user_account a');
		$this->db->join('core_account_type b', 'a.user_account_type_id = b.account_type_id ');
		$this->db->where('a.user_account_username', $username);
		$query = $this->db->get();
		$res = '';
		if ($query){
			$res = $query->row();
		}
		return $res;
	}
}
