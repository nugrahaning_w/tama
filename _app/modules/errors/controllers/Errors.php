<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends Public_Controller {

	public function index()
	{
		$data = array();
		$this->output->set_status_header('404');
		$this->template->title('Not Found');
		$this->template->content('errors/publik_404', $data);
		$this->template->show( THEMES_PUBLIC . 'index' );
	}

	public function coming_soon()
	{
		$data = array();
		$this->template->title('Coming Soon');		
		$this->template->show('errors/coming_soon');
	}

}
