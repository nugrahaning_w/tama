<div class="background">
    <div class="fp-table content-404">
        <div class="fp-table-cell center">

            <div class="container">
                <div class="margin-bottom-40">
                    <h1 class="size-150"><span>404</span></h1>

                </div>
                <h3>ohh! page not found</h3>

                <p>
                    <span>It seems we can’t find what you’re looking for. Perhaps searching can help or go back to</span>
                    <a href="<?php echo base_url(); ?>" class="margin-left-10" style="color:blue;">Home Page</a>
                </p>
            </div>

        </div>
    </div>
</div>