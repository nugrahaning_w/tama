<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?php echo template_echo('title') ?></title>
        <meta content="Responsive admin theme build on top of Bootstrap 4" name="description" />
        <meta content="Themesdesign" name="author" />
        <link rel="shortcut icon" href="<?php echo THEMES_ADMIN; ?>assets/images/favicon.ico">

        <link href="<?php echo THEMES_ADMIN; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEMES_ADMIN; ?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEMES_ADMIN; ?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo THEMES_ADMIN; ?>assets/css/style.css" rel="stylesheet" type="text/css">

    </head>

    <body>

        <!-- Begin page -->
        
        <div class="home-btn d-none d-sm-block">
                <a href="<?php echo base_url(); ?>" class="text-dark"><i class="fas fa-home h2"></i></a>
            </div>


            <div class="account-pages">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="text-center mb-5">
                                    <div class="mb-5">
                                        <h2>Coming Soon</h2>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
        
                        <!-- end row -->

                    </div>
                </div>
        
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo THEMES_ADMIN; ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo THEMES_ADMIN; ?>assets/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo THEMES_ADMIN; ?>assets/js/metismenu.min.js"></script>
        <script src="<?php echo THEMES_ADMIN; ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo THEMES_ADMIN; ?>assets/js/waves.min.js"></script>

        <!-- countdown js -->
        <script src="../plugins/countdown/jquery.countdown.min.js"></script>
        <script src="<?php echo THEMES_ADMIN; ?>assets/pages/countdown.int.js"></script>        

        <!-- App js -->
        <script src="<?php echo THEMES_ADMIN; ?>assets/js/app.js"></script>
        
    </body>

</html>