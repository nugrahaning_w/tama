<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rejected extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->show(); 
	}

	function show() {
		$data = [];
        $data['breadcrumb'] = [];
        $data['is_ready'] = false;
        $data['product'] = [];
		$params = [
			'table' => 'sys_product a',
			'select' => 'a.product_name, a.product_id',
		];
		$query = $this->crud->get_data( $params ); 
		$data['product'] = $query->result_array();
		$this->template->title( 'Dashboard Foreman' );
		$this->template->content( 'foreman/form_rejected', $data );
		$this->template->show( 'themes/admin/index' );
	}
	function save_rejected_report() {
		$id = $this->input->post("id_product");
		$cacat_gap = $this->input->post("cacat_gap");
		$cacat_wire = $this->input->post("cacat_wire");
		$cacat_electro = $this->input->post("cacat_electro");
		$cacat_insulator = $this->input->post("cacat_insulator");
		$cacat_total = $this->input->post("total_cacat");
		$date = date("Y-m-d");
		$params = [
			'table' => 'sys_rejected',
			'select' => 'rejected_id',
			'where' => [
				'rejected_date_input' => $date,
				'rejected_product_id' => $id
            ]
		];
		$query = $this->crud->get_data( $params ); 
		$afected = $query->num_rows();

		$data = array(
			'rejected_product_id' => $id,
			'rejected_cacat_wire' => $cacat_wire,
			'rejected_cacat_gap' => $cacat_gap,
			'rejected_cacat_electro' => $cacat_electro,
			'rejected_cacat_insulator' => $cacat_insulator,
			'rejected_total_cacat' => $cacat_total,
			'rejected_date_input' =>$date
		);
		$table = 'sys_rejected';
		if ($afected != 0) {
			$where = array(
				'rejected_date_input' => $date,
				'rejected_product_id' => $id
			);
		}
		$update = $this->crud->save_data($table, $data, $where);
		if ($update) {
			$res = [
				'code' => '200',
				'data' => '',
				'message' => '',
			];
			
			die( json_encode( $res ) );
		}
	}
	function cek_reject()
	{
		$id = $this->input->post('id_product');
		$params = [
			'table' => 'sys_rejected',
			'select' => 'rejected_id, rejected_cacat_wire, rejected_cacat_gap, rejected_cacat_electro, rejected_cacat_insulator, rejected_total_cacat',
			'where' => [
				'rejected_date_input' => date("Y-m-d"),
				'rejected_product_id' => $id
            ]
		];
		$query = $this->crud->get_data( $params );
        if ($query->num_rows() == 1) {
            $res = [
				'code' => '200',
				'data' => $query->row(),
				'message' => 'Laporan reject ini Sudah terisi !',
			];
        } else {
			$res = [
				'code' => '400',
				'data' => '',
				'message' => '',
			];
		}
		die( json_encode( $res ) );
	}
}