<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->show(); 
	}

	function show() {
		$data = [];
        $data['breadcrumb'] = [];
        $data['product'] = [];
		$params = [
			'table' => 'sys_product a',
			'select' => 'a.product_name, a.product_id',
		];
		$query = $this->crud->get_data( $params ); 
		$data['product'] = $query->result_array();
		$this->template->title( 'Dashboard Foreman' );
		$this->template->content( 'foreman/form_report', $data );
		$this->template->show( 'themes/admin/index' );
	}
	function cek_report()
	{
		$id = $this->input->post('id_product');
		$params = [
			'table' => 'sys_report_product',
			'select' => 'report_id, report_target_count, report_rejected_count, report_repair_count, report_scrap_count, report_total_count, report_note',
			'where' => [
				'report_date_input' => date("Y-m-d"),
				'report_product_id' => $id
            ]
		];
		$query = $this->crud->get_data( $params );
        if ($query->num_rows() == 1) {
            $res = [
				'code' => '200',
				'data' => $query->row(),
				'message' => 'Laporan Product ini Sudah terisi !',
			];
        } else {
			$params = [
				'table' => 'sys_rejected',
				'select' => 'rejected_id, rejected_total_cacat',
				'where' => [
					'rejected_date_input' => date("Y-m-d"),
					'rejected_product_id' => $id
				]
			];
			$query = $this->crud->get_data( $params );
			$res = [
				'code' => '201',
				'data' => $query->row(),
				'message' => '',
			];
		}
		die( json_encode( $res ) );
	}
	function save_rejected_report() {
		$id = $this->input->post("id_product");
		$target_count = $this->input->post("target_count");
		$rejected_count = $this->input->post("reject_count");
		$scrap = $this->input->post("scrap_count");
		$repair = $this->input->post("repair_count");
		$total_count = $this->input->post("total_count");
		$note = $this->input->post("note");
		$date = date("Y-m-d");
		$params = [
			'table' => 'sys_report_product a',
			'select' => 'a.report_id',
			'where' => [
				'a.report_date_input' => $date,
				'a.report_product_id' => $id
            ]
		];
		$query = $this->crud->get_data( $params ); 
		$afected = $query->num_rows();

		$paramsget = [
			'table' => 'sys_rejected',
			'select' => 'rejected_id, rejected_product_id',
			'where' => [
				'rejected_date_input' => $date,
				'rejected_product_id' => $id
            ]
		];
		$queryget = $this->crud->get_data( $paramsget );
		$data = array(
			'report_product_id' => $id,
			'report_target_count' => $target_count,
			'report_rejected_id' => $queryget->row()->rejected_id,
			'report_rejected_count' => $rejected_count,
			'report_total_count' => $total_count,
			'report_repair_count' => $repair,
			'report_scrap_count' => $scrap,
			'report_note' => $note,
			'report_date_input' =>$date
		);
		$table = 'sys_report_product';
		if ($afected != 0) {
			$where = array(
				'report_date_input' => $date,
				'report_product_id' => $id
			);
		}
		$update = $this->crud->save_data($table, $data, $where);
		if ($update) {
			$res = [
				'code' => '200',
				'data' => '',
				'message' => '',
			];
			
			die( json_encode( $res ) );
		}
	}
}