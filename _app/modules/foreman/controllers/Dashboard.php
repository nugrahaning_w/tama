<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Backend_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->show(); 
	}

	function show() {
		$data = [];
		$data['breadcrumb'] = [];
		$this->template->title( 'Dashboard Supervisor' );
		$this->template->content( 'foreman/dashboard_view' );
		$this->template->show( 'themes/admin/index' );
	}
}