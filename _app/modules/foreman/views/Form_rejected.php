

<!-- jadi -->
<div class="row">
	<div class="col-12">
		<div class="livizo-notices-wrapper form-notice"></div>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<form>
					<h4 class="header-title">Form Reject Product</h4>
					<p class="sub-title"></p>
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>Nama Produk</label>
								<select class="form-control name_product" name="name_product" required>
                                    <option selected="selected" value="" style="display:none;">Pilih produk</option>
											<?php
											foreach ($product as $p) { ?>
												<option value='<?php echo $p['product_id']; ?>'><?php echo $p['product_name']; ?></option>
											<?php
											} ?>
								</select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Jumlah Cacat Center Wire Projection</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control cacat_wire" name="cacat_wire" min="0" required value="0">
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Jumlah Cacat Lebar GAP</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control cacat_gap" name="cacat_gap" min="0"  required value="0">
							</div>
						</div>
                        <div class="col-12">
							<div class="form-group">
								<label>Jumlah Cacat Center Elektro</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control cacat_electro" name="cacat_electro" min="0"  required value="0">
							</div>
						</div> 
                        <div class="col-12">
							<div class="form-group">
								<label>Jumlah Cacat Hidung Insulator</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control cacat_insulator" name="cacat_insulator" min="0"  required value="0">
							</div>
						</div>  
					</div>
				</form>
			</div>
		</div>
	</div>
    <div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<form>
					<h4 class="header-title">Total Reject Product</h4>
					<p class="sub-title"></p>
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>Jumlah Product Reject</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control total_reject" name="total_reject" min="0" required disabled value="0">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal_alert"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="center">
				<h4>Ooops....<span class="grt"></span> </h4>
			</div>
			<div class="center">
					<h5>Tidak ada perubahan</h5>
				</div>
			<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
				<span id="preloader-delete"></span>
				<div class = "center">
					<button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link">Cancel</button>
				</div>
				
			</div>
		</div>
	</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card m-b-30">
			<div class="card-body shadow-box">
				<ul class="nav nav-pills">
					<li class="nav-item"  style="margin:auto;">
						<button class ="nav-link active tambah_reject"> Save</button>					
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script>
$('input.cacat_wire').on('input', function() {
	let wire = parseInt($(this).val())
	let gap = parseInt($("input.cacat_gap").val())
    let electro = parseInt($("input.cacat_electro").val())
    let insulator = parseInt($("input.cacat_insulator").val())
	let total = wire + gap + electro + insulator
	$("input.total_reject").val(total)
});
$('input.cacat_gap').on('input', function() {
	let wire = parseInt($("input.cacat_wire").val())
	let gap = parseInt($(this).val())
    let electro = parseInt($("input.cacat_electro").val())
    let insulator = parseInt($("input.cacat_insulator").val())
	let total = wire + gap + electro + insulator
	$("input.total_reject").val(total)
});
$('input.cacat_electro').on('input', function() {
	let wire = parseInt($("input.cacat_wire").val())
	let gap = parseInt($("input.cacat_gap").val())
    let electro = parseInt($(this).val())
    let insulator = parseInt($("input.cacat_insulator").val())
	let total = wire + gap + electro + insulator
	$("input.total_reject").val(total)
});
$('input.cacat_insulator').on('input', function() {
	let wire = parseInt($("input.cacat_wire").val())
	let gap = parseInt($("input.cacat_gap").val())
    let electro = parseInt($("input.cacat_electro").val())
    let insulator = parseInt($(this).val())
	let total = wire + gap + electro + insulator
	$("input.total_reject").val(total)
});

$('select.name_product').on('change', function() {
	let data = {
		id_product : $(this).val(),
	}
  $.ajax({
		url: "<?php echo base_url()?>foreman/Rejected/cek_reject",
		type: "POST",
		data: data,
		dataType: "json",
		success: function( res ){
            if (res.code == "200") {
                $('input.cacat_wire').val(res.data.rejected_cacat_wire)
                $('input.cacat_gap').val(res.data.rejected_cacat_gap)
                $('input.cacat_electro').val(res.data.rejected_cacat_electro)
                $('input.cacat_insulator').val(res.data.rejected_cacat_insulator)
                $('input.total_reject').val(res.data.rejected_total_cacat) 
            }
            else {
                $('input.cacat_wire').val("0")
                $('input.cacat_gap').val("0")
                $('input.cacat_electro').val("0")
                $('input.cacat_insulator').val("0")
                $('input.total_reject').val("0")
            }
		},
        error: function() {
            $('input.cacat_wire').val("0")
			$('input.cacat_gap').val("0")
			$('input.cacat_electro').val("0")
			$('input.cacat_insulator').val("0")
            $('input.total_reject').val("0")
        }
	});
});

$(document).on( "click", "button.tambah_reject", function(){
    if ($("select.name_product").val() == "0" || $("select.name_product").val() == 0){
        alert("Nama Product Harus diisi")
    }else{
        let data = {
            id_product : $("select.name_product").val(),
            cacat_wire : $("input.cacat_wire").val(),
            cacat_gap : $("input.cacat_gap").val(),
            cacat_electro : $("input.cacat_electro").val(),
            cacat_insulator : $("input.cacat_insulator").val(),
            total_cacat : $("input.total_reject").val()
        }
        $.ajax({
            url: "<?php echo base_url()?>foreman/Rejected/save_rejected_report",
            type: "POST",
            data: data,
            dataType: "json",
            success: function( res ){
                location.reload()
            },
            error: function(){
                // alert("error");
				$("#modal_alert").modal( "show" );
            }
        });
    }
});
</script>
