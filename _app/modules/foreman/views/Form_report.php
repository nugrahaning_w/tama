
<!-- jadi -->
<div class="row">
	<div class="col-12">
		<div class="livizo-notices-wrapper form-notice"></div>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<form class="livizo" method="post" action="<?= base_url('product/product_tambah'); ?>">
					<h4 class="header-title">Form Report Product</h4>
					<p class="sub-title"></p>
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>Nama Produk</label>
								<select class="form-control name_product" name="name_product" required>
                                    <option selected="selected" value="" style="display:none;">Pilih produk</option>
											<?php
											foreach ($product as $p) { ?>
												<option value='<?php echo $p['product_id']; ?>'><?php echo $p['product_name']; ?></option>
											<?php
											} ?>
								</select>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Jumlah Target Produksi</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control target_count" name="target_count" min="0" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Jumlah Produk Reject</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control reject_count" name="reject_count" min="0"  required>
							</div>
						</div>  
						<div class="col-12">
							<div class="form-group">
								<label>Jumlah Produksi</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control total_count" name="total_count" min="0" required disabled>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="card">
			<div class="card-body shadow-box">
				<form>
					<h4 class="header-title"> </h4>
					<p class="sub-title"></p>
					<div class="row">
						<div class="col-12">
							<div class="form-group">
								<label>Akan di repair</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control repaired" name="repaired" min="0" required>
							</div>
						</div>
						<div class="col-12">
							<div class="form-group">
								<label>Akan di scrap</label>
								<input type="number" placeholder="" data-mask="999.999.999.9999" class="numb form-control scraped" name="scraped" min="0"  required>
							</div>
						</div> 
						<div class="col-12">
								<div class="form-group"> 
                                    <label for=""></label>                                                     
									<textarea id="note" name="note" placeholder="Enter text ..." style="width: 500px; height: 200px" required></textarea>
								</div>                               
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal fade" id="modal_alert"  data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="center">
					<h4>Ooops....<span class="grt"></span> </h4>
				</div>
				<div class="center">
						<h5>Tidak ada perubahan</h5>
					</div>
				<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
					<span id="preloader-delete"></span>
					<div class = "center">
						<button type="button" class="btn btn-info" data-dismiss="modal" id="delete_cancel_link">Cancel</button>
					</div>
					
				</div>
			</div>
		</div>
		</div>
	</div>
		
</div>
<div class="row">
	<div class="col-12">
		<div class="card m-b-30">
			<div class="card-body shadow-box">
				<ul class="nav nav-pills">
					<li class="nav-item"  style="margin:auto;">
						<button class ="nav-link active tambah_reject"> Save</button>					
					</li>

				</ul>
			</div>
		</div>
	</div>
</div>
<script>
$('input.target_count').on('input', function() {
	let target = $(this).val()
	let reject = $("input.reject_count").val()
	let total = target - reject
	$("input.total_count").val(total)
});
$('input.reject_count').on('input', function() {
	let target = $("input.target_count").val()
	let reject = $(this).val()
	let total = target - reject
	$("input.total_count").val(total)
});
$('select.name_product').on('change', function() {
	let data = {
		id_product : $(this).val(),
	}
  $.ajax({
		url: "<?php echo base_url()?>foreman/Report/cek_report",
		type: "POST",
		data: data,
		dataType: "json",
		success: function( res ){
			if (res.code == "200") {
				console.log(res.data);
				$('input.target_count').val(res.data.report_target_count)
				$('input.reject_count').val(res.data.report_rejected_count)
				$('input.total_count').val(res.data.report_total_count)
				$('input.repaired').val(res.data.report_repair_count)
				$('input.scraped').val(res.data.report_scrap_count)
				$('textarea#note').val(res.data.report_note)
			} else {
				if (res.data != null) {
					$('input.reject_count').val(res.data.rejected_total_cacat)
				} else {
					$('input.target_count').val('')
					$('input.reject_count').val('')
					$('input.total_count').val('')
					$('input.repaired').val('')
					$('input.scraped').val('')
					$('textarea#note').val('')
				}
				
			}
			
		},
	});
});

$(document).on( "click", "button.tambah_reject", function(){
	let data = {
		id_product : $("select.name_product").val(),
		target_count : $("input.target_count").val(),
		reject_count : $("input.reject_count").val(),
		total_count : $("input.total_count").val(),
		scrap_count : $("input.scraped").val(),
		repair_count : $("input.repaired").val(),
		note : $("textarea#note").val()
	}
	$.ajax({
		url: "<?php echo base_url()?>foreman/Report/save_rejected_report",
		type: "POST",
		data: data,
		dataType: "json",
		success: function( res ){
			location.reload()
		},
		error: function(){
			// alert("error");
			$("#modal_alert").modal( "show" );
		}
	});
	
});
</script>
