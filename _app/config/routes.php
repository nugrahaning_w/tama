<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'auth/login';
$route['404_override'] = 'errors';
$route['translate_uri_dashes'] = FALSE;