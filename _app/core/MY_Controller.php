<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        define('IMAGES', site_url('uploads/images/'));
        $this->set_language();
    }

    public function createRespon($params)
    {
        $pesan = array(
            'status_code' => $params['code'],
            'message' => $params['msg'],
            'url' => $params['url']
        );

        $pesan = array(
            'status_code' => $params['code'],
            'message' => $params['msg'],
            'data' => $params['data'],
            'url' => $params['url']
        );
        return json_encode($pesan);
    }

    public function get_theme($type = null)
    {
        $params = [
            'table' => 'core_configuration',
            'select' => 'configuration_value_is_json, configuration_value',
            'where' => [
                'configuration_type' => 'themes'
            ]
        ];
        $theme_conf = $this->crud->get_data($params);
        if ($theme_conf->row('configuration_value_is_json')) {
            $theme_conf = json_decode($theme_conf->row('configuration_value'), true);
            $theme_login = $theme_conf[$type];
        } else {
            $theme_login = $theme_conf->row('configuration_value');
        }
        return $theme_login;
    }

    public function set_language(){
        $set_language = $this->session->userdata('language');
        if ($set_language) {
            $this->lang->load('app_lang',$set_language);
        } else {
            $this->lang->load('app_lang','indonesia');
        }
    }
}
