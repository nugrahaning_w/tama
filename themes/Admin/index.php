<!-- <?php //disp($_SESSION['user_info']); die;?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<title><?php echo template_echo('title') ?></title>
	<meta content="Themesdesign" name="author" />
	<link rel="shortcut icon" href="<?php echo base_url(THEMES_ADMIN); ?>assets/images/ico.jpeg">

	<link href="<?php echo base_url(THEMES_ADMIN); ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(THEMES_ADMIN); ?>/assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(THEMES_ADMIN); ?>/assets/css/icons.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(THEMES_ADMIN); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
	<!-- jQuery -->
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/jquery.min.js"></script>
	<!-- addOn -->
	<script src="<?php echo $addons; ?>conversi.js"></script>
	<?php
	if (isset($header_script)) {
		echo $header_script;
	}
	?>
</head>

<body>
	<div id="wrapper">
		<div class="topbar">
			<div class="topbar-left">
				<a href="index.html" class="logo">
					<span class="logo-light">
						<a href="<?php echo base_url(''); ?>">
							<img style="padding-right: 70px;" width="80%" height="auto" src="<?php echo base_url(THEMES_ADMIN); ?>/assets/images/ico.jpeg" class="custom-logo" alt="PT. Multi Prima Sejahtera"> </a>
					</span>
				</a>
			</div>

			<nav class="navbar-custom">
				<ul class="navbar-right list-inline float-right mb-0">
					
					<li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
						<a class="nav-link waves-effect" href="#" id="btn-fullscreen">
							<i class="mdi mdi-arrow-expand-all noti-icon"></i>
						</a>
					</li>
					<li class="dropdown notification-list list-inline-item">
						<div class="dropdown-menu dropdown-menu-right dropdown-menu-animated dropdown-menu-lg px-1">
							<h6 class="dropdown-item-text">
								Notifikasi
							</h6>
							<div class="slimscroll notification-item-list" style="height:400px;">
								</div>
							<a href="javascript:void(0);" class="dropdown-item text-center notify-all text-primary">
								View all <i class="fi-arrow-right"></i>
							</a>
						</div>
					</li>
					<li class="dropdown notification-list list-inline-item">
						<div class="dropdown notification-list nav-pro-img">
							<a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
								<img src="<?php echo base_url(THEMES_ADMIN) . 'assets/images/profile.png'; ?>" alt="user" class="rounded-circle">
							</a>
							<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
								<a class="dropdown-item" href="#"><i class="mdi mdi-account-circle"></i><?= $this->session->userdata('user_info')['user_username']; ?></a>
								<a id="ubah-password" class="dropdown-item d-block" href="#"><i class="mdi mdi-lock"></i> Ubah Password</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item text-danger" href="<?= base_url('auth/logout') ?>"><i class="mdi mdi-power text-danger"></i> Logout</a>
							</div>
						</div>
					</li>
				</ul>

				<ul class="list-inline menu-left mb-0">
					<li class="float-left">
						<button class="button-menu-mobile open-left waves-effect">
							<i class="mdi mdi-menu"></i>
						</button>
					</li>
				</ul>
			</nav>
		</div>
		<div class="left side-menu">
			<div class="slimscroll-menu" id="remove-scroll">
				<div id="sidebar-menu">
					<ul class="metismenu" id="side-menu">
						<?php;
						if ($this->session->user_info['user_type'] == 'Admin') {
						?>
							<li class="menu-title">Menu Admin</li>
							<li>
								
								<a href="<?php echo base_url('admin/dashboard'); ?>" class="waves-effect disable">
									<i class="fa fa-cubes"></i><span class="badge badge-success badge-pill float-right"></span> <span> Dashboard </span>
								</a>
								<a href="<?php echo base_url('users/admin'); ?>" class="waves-effect">
									<i class="fa fa-address-card"></i><span class="badge badge-success badge-pill float-right"></span> <span> Users </span>
								</a>
								<a href="<?php echo base_url('products/admin'); ?>" class="waves-effect">
									<i class="fa fa-bolt"></i><span class="badge badge-success badge-pill float-right"></span> <span> Products </span>
								</a>

							</li>
						<?php
						}

						if ($this->session->user_info['user_type'] == 'Supervisor') {
						?>
							<li class="menu-title">Menu Supervisor</li>
							<li>
								<a href="<?php echo base_url('spv/dashboard'); ?>" class="waves-effect disable">
									<i class="fa fa-cubes"></i><span class="badge badge-success badge-pill float-right"></span> <span> Dashboard </span>
								</a>
								<a href="<?php echo base_url('spv/rejected');?>" class="waves-effect">
									<i class="fa fa-bolt"></i><span class="badge badge-success badge-pill float-right"></span> <span> Laporan Produksi </span>
								</a>
								
							</li>
						<?php
						}
						if ($this->session->user_info['user_type'] == 'Foreman') {
							?>
								<li class="menu-title">Menu Foreman</li>
								<li>
									<a href="<?php echo base_url('foreman/dashboard'); ?>" class="waves-effect disable">
										<i class="fa fa-cubes"></i><span class="badge badge-success badge-pill float-right"></span> <span> Dashboard </span>
									</a>
									<a href="<?php echo base_url('foreman/rejected'); ?>" class="waves-effect">
										<i class="fa fa-cubes"></i><span class="badge badge-success badge-pill float-right"></span> <span> Laporan Produk Reject</span>
									</a>
									<a href="<?php echo base_url('foreman/report'); ?>" class="waves-effect">
									<i class="fa fa-address-card"></i><span class="badge badge-success badge-pill float-right"></span> <span> Laporan Produksi</span>
									</a>
									
								</li>
							<?php } ?>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="page-title-box">
						<div class="row align-items-center">
							<div class="col-sm-6">
								<ol class="breadcrumb float-right">
									<?php

									$count = count($breadcrumb);
									$i = 1;
									foreach ($breadcrumb as $name => $url) {
										echo '
									<li class="breadcrumb-item ' . (($i == $count) ? 'active' : null) . '">
										<a href="' . base_url($url) . '">' . $name . '</a>
									</li>';
										$i++;
									}
									?>
								</ol>
							</div>
						</div>
					</div>
					<?php
					if (!empty($content)) echo $content;
					?>
				</div>
			</div>
			
			<footer class="footer">
				© 2020 PT. Multi Prima Sejahtera
			</footer>
		</div>
	</div>
	<!-- jQuery  -->
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/metismenu.min.js"></script>
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/jquery.slimscroll.js"></script>
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/waves.min.js"></script>
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/app.js"></script>
	<script src="<?php echo base_url(THEMES_ADMIN); ?>/assets/js/Chart.min.js"></script>

	<script>
	</script>
	<?php
	if (isset($footer_script)) {
		echo $footer_script;
	}
	?>
</body>

</html>
