<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo template_echo('title'); ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url( THEMES_LOGIN ); ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url( THEMES_LOGIN ); ?>css/main.css">
<!--===============================================================================================-->
</head>
<body>
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?php echo base_url( THEMES_LOGIN ); ?>images/Untitled-1.png" alt="IMG">
				</div>
				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Login Sistem Informasi Inspeksi Busi
					</span>
					<div id="notif"></div>
					<div class="wrap-input100 validate-input" data-validate = "Username is required">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<input type="button" class="login100-form-btn" value="Login">
						</input>
					</div>

					<div class="text-center p-t-12">
						
					</div>

					<div class="text-center p-t-136">
						
					</div>
				</form>
			</div>
		</div>
    </div>
	
	

	
<!--===============================================================================================-->	
	<script src="<?php echo base_url( THEMES_LOGIN ); ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url( THEMES_LOGIN ); ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url( THEMES_LOGIN ); ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url( THEMES_LOGIN ); ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url( THEMES_LOGIN ); ?>vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		"use strict";

		var input = $('.validate-input .input100');
		$('.login100-form-btn').on( 'click', function(){
			var check = true;
			for(var i=0; i<input.length; i++) {
				if(validate(input[i]) == false){
					showValidate(input[i]);
					check=false;
				}
			}
			if (check == true) {
				var username = $('input[name="username"]').val();
				var pass = $('input[name="pass"]').val();
				var data = {
					username : username,
					pass : pass
				}	
				$.ajax({
					url : '<?php echo base_url( 'auth/login/act_login' );?>',
					data : data,
					method : 'POST',
					dataType : "JSON",            
					success : function( response ){
						$('#notif').removeClass('alert alert-danger alert-success').empty();
						if ( response.code == '200' ){                                  
							$('#notif').addClass( 'alert alert-success' ).html( response.msg );
							if( response.url != '' ) {
								location.href = response.url;
							}
						} else {
							$('#notif').addClass( 'alert alert-danger' ).html( response.msg );
						} 
					},
					error : function(xhr){
					}
				});
			}
		})
		$('.validate-form .input100').each(function(){
			$(this).focus(function(){
			hideValidate(this);
			});
		});

		$('.js-tilt').tilt({
			scale: 1.1
		})

		function validate (input) {
			if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
				if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
					return false;
				}
			}
			else {
				if($(input).val().trim() == ''){
					return false;
				}
			}
		}

		function showValidate(input) {
			var thisAlert = $(input).parent();
			$(thisAlert).addClass('alert-validate');
		}

		function hideValidate(input) {
			var thisAlert = $(input).parent();
			$(thisAlert).removeClass('alert-validate');
		}

		
	</script>
</body>
</html>