$(document).ready(function(){
	
});

/* CONVERT TO NOMINAL */
function convertToNominal(bilangan, action = false)
{
	if(bilangan != null){

		if(bilangan.length == 0){
			return 0;
		}
		var frontHtml = '';                
		if(action == true){
			frontHtml = '$ ';
		}else{
			frontHtml = 'Rp ';
		}

		return frontHtml + convertRibuan(bilangan);

	}else{
		return 0;
	}

}

/* CONVERT ANGKA RIBUAN */
function convertRibuan(bilangan){                
	if(Math.floor(bilangan) == bilangan && $.isNumeric(bilangan)){
		var reverse = bilangan.toString().split('').reverse().join(''),
		ribuan  = reverse.match(/\d{1,3}/g);
		return ribuan.join('.').split('').reverse().join('');
	}else{
		return bilangan;
	}
}

/* API KURS */
function api_kurs(bilangan = '')
{
	if(bilangan != ''){

		let bank = 'bi'
		$.ajax({
			url : 'https://kurs.web.id/api/v1/' + bank,
			method : 'GET',
			success : function(data){
				console.log(data);
			},
			error : function(xhr){
				console.log('Error API KURS');
			}
		});
	}
}